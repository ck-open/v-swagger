
knife4j 版本：3.0 配置依赖封装 匹配Spring Boot 版本：2.5.6及以下

application.yml 中配置如下
swagger:
  enabled: true             # 是否启用文档
  auto-browser: true        # 自动打开浏览器
  title: CK接口文档         # 文档标题
  description: CK接口文档   # 描述
  version: 1.0             # 版本
  base-package: com.ck     # 接口扫描的包
  contact:                # 作者信息
    name: cyk
    url: http://ck.xx
    email: xx@ck.cn
  terms-of-service-url: http://localhost:%s/  # 服务信息地址
