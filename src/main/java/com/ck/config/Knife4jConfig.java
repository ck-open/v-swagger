package com.ck.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.event.EventListener;
import org.springframework.util.ObjectUtils;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * @ClassName Knife4jConfig
 * @Description Knife4j 配置
 * @Author Cyk
 * @Version 1.0
 * @since 2022/7/13 10:58
 **/
@Slf4j
@EnableOpenApi
@Configuration
@Import({SwaggerProperties.class})
public class Knife4jConfig {

    @Value("${spring.application.name:Application}")
    private String applicationName;

    @Value("${server.port:?}")
    private String port;

    @Resource
    private SwaggerProperties swaggerProperties;

    /**
     * @param
     * @return ApiInfo
     * @description 配置基本信息
     * @author Cyk
     * @since 14:11 2022/7/13
     **/
    public ApiInfo getApiInfo() {
        return new ApiInfoBuilder()
                .title(applicationName + "-Swagger-Bootstrap-UI")
                .description("Api接口文档")
                .description("<div style='font-size:14px;color:blue;'>Swagger-Bootstrap-UI-Demo RESTFUL APIs</div>")
                .termsOfServiceUrl(String.format("http://localhost:%s/", port))
                .contact(new Contact("CK", "http://ck.***", "**@ck.cn"))
                .version("1.0.0")
                .build();
    }

    /**
     * @return Docket
     * @description 配置文档生成
     * @author Cyk
     * @since 14:12 2022/7/13
     **/
    @Bean
    @ConditionalOnMissingBean(Docket.class)
    public Docket createRestApi(SwaggerProperties swaggerProperties) {
        String basePackage = this.getClass().getPackage().getName();
        if (basePackage.contains(".")) basePackage = basePackage.substring(0, basePackage.indexOf("."));
        ApiInfo apiInfo = getApiInfo();
        if (!ObjectUtils.isEmpty(swaggerProperties)) {
            apiInfo = swaggerProperties.build(apiInfo);
        }

        return new Docket(DocumentationType.SWAGGER_2)
                .enable(swaggerProperties.getEnabled())
                .apiInfo(apiInfo)
                .select()
                .apis(RequestHandlerSelectors.basePackage(basePackage))
                .paths(PathSelectors.any())
                .build();
    }

    /**
     * @description 启动后自动打开浏览器
     * @author Cyk
     * @since 14:37 2022/7/15
     * @param
     * @return void
     **/
    @EventListener({ApplicationReadyEvent.class})
    void applicationReadyEvent() {
        if (swaggerProperties.getAutoBrowser() && swaggerProperties.getEnabled()) {
            // 这里需要注url:端口号+测试类方法名
            String url = String.format("http://localhost:%s/doc.html", port);
            log.info("应用已经准备就绪 ... 启动浏览器  " + url);
            Runtime runtime = Runtime.getRuntime();
            try {
                runtime.exec("rundll32 url.dll,FileProtocolHandler " + url);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
